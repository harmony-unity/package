@echo off

@REM Check if the correct number of arguments is provided
if "%~3"=="" (
    echo Usage: %0 "program_path" "project_path" "output_dir"
    exit /b 1
)

@REM Assigning arguments to variables
set "program_path=%~1"
set "project_path=%~2"
set "output_dir=%~3"

REM Execute the program and redirect output to the log file
echo Running code generator. Please wait ...
"%program_path%" --input-dir "%project_path%" --output-dir "%output_dir%" --unattended --verbose > "%output_dir%/Generator.log" 2> "%output_dir%/GeneratorErr.log"