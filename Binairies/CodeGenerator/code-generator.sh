#!/bin/bash

# Check if the correct number of arguments is provided
if [ "$#" -ne 3 ]; then
    echo "Usage: $0 <program_path> <project_path> <output_dir>"
    exit 1
fi

# Assigning arguments to variables
program_path="$1"
project_path="$2"
output_dir="$3"

# Execute the program and redirect output to the log file
echo "Running code generator. Please wait ..."
"$program_path" --input-dir "$project_path" --output-dir "$output_dir" --unattended --verbose > "$output_dir/Generator.log" 2> "$output_dir/GeneratorErr.log"