﻿using System.IO;

namespace Harmony
{
    public static class HarmonyPkg
    {
        public static readonly string PackagePath = Path.GetFullPath("Packages/ca.harmony");
        public static readonly string GizmoPath = "Packages/ca.harmony/Runtime/Gizmos";
#if UNITY_EDITOR_WIN
        public static readonly string GeneratorScriptPath = Path.Combine(PackagePath, "Binairies/CodeGenerator/code-generator.bat");
        public static readonly string GeneratorBinPath = Path.Combine(PackagePath, "Binairies/CodeGenerator/code-generator.exe");
#else
        public static readonly string GeneratorScriptPath = Path.Combine(PackagePath, "Binairies/CodeGenerator/code-generator.sh");
        public static readonly string GeneratorBinPath = Path.Combine(PackagePath, "Binairies/CodeGenerator/code-generator");
#endif
    }
}