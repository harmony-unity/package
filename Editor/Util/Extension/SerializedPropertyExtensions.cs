﻿using System;
using System.Reflection;
using UnityEditor;

namespace Harmony
{
    public static class SerializedPropertyExtensions
    {
        public static bool NeedRefresh(this SerializedProperty property)
        {
            //If this throws an Exception, the property is invalid.
            try
            {
                var ignore = property.name;
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }
    }
}