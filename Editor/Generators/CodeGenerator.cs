﻿using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Harmony
{
    public class CodeGenerator : EditorWindow
    {
        [MenuItem("Tools/Harmony/Code Generator...", priority = 100)]
        public static void GenerateConstClasses()
        {
            GetWindow<CodeGenerator>(false, "Harmony Code Generator", true).Show();
        }

        private void OnEnable()
        {
            minSize = new Vector2(300, 90);
        }

        private void OnGUI()
        {
            if (GUILayout.Button("Generate code")) GenerateCode();
            if (GUILayout.Button("Open generated folder")) OpenGeneratedFolder();
            
            EditorGUILayout.HelpBox("Logs are located alongside the generated code.", MessageType.Info);
        }

        private void OnInspectorUpdate()
        {
            Repaint();
        }

        private void GenerateCode()
        {
            Task.Run(() =>
            {
                var pathToScript = HarmonyPkg.GeneratorScriptPath;
                var pathToExe = HarmonyPkg.GeneratorBinPath;
                
                var pathToProjectDirectory = Path.GetFullPath(Path.Combine(Application.dataPath, ".."));
                var pathToGeneratedDirectory = Path.Combine(pathToProjectDirectory, "Assets/Generated/Harmony");

                var processStartInfo = new ProcessStartInfo
                {
                    FileName = pathToScript,
                    CreateNoWindow = false,
                    UseShellExecute = true,
                    Arguments = $"\"{pathToExe}\" \"{pathToProjectDirectory}\" \"{pathToGeneratedDirectory}\"",
                };

                Process.Start(processStartInfo).WaitForExit();
                RefreshAssets();
            });
        }

        private static void OpenGeneratedFolder()
        {
            var pathToProjectDirectory = Path.GetFullPath(Path.Combine(Application.dataPath, ".."));
            var pathToGeneratedDirectory = Path.Combine(pathToProjectDirectory, "Assets/Generated/Harmony");
            
            var processStartInfo = new ProcessStartInfo
            {
                FileName = pathToGeneratedDirectory
            };

            Process.Start(processStartInfo);
        }
        
        private static void RefreshAssets()
        {
            AssetDatabase.Refresh();
        }
    }
}