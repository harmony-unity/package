# Harmony Unity Package

This is the Harmony Unity Package. This contains some useful functionality, including a code generator that generates
various useful classes.

## Installation

In your *Unity* project, open the *Package Manager*. Add this package using the following *git* url :

```text
https://gitlab.com/harmony-unity/package.git
```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
